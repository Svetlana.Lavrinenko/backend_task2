
import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {

        Scanner in = new Scanner(System.in);
        System.out.print("Температура воздуха: ");
        int temperature = in.nextInt();
        System.out.print("Скорость ветра: ");
        int wind = in.nextInt();
        System.out.print("Идет дождь (да/нет): ");
        String rain = in.next();

        if (temperature >= 5 && wind <= 5 && rain.equalsIgnoreCase("нет")) {
            System.out.println("Можно выходить на улицу");
        } else {
            System.out.println();
            System.out.println("Выходить на улицу не стоит");
        }
        in.close();


    }
}
